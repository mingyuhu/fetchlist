FETCH COMMAND
=============

Using hydro's fetch command, a UOW is made in the current dir [default] or a directed directory. 

This UOW is tracked by a fetchlist, that is automatically recorded, by a POST request anytime fetch is called. 

REQUEST
-------

**URL**: "http://127.0.0.1:5000" *change*

**request**: "POST"
    *encode*: JSON  
    
    *body*:
        * Username  (username)
        * Hostname  (hostname)
        * Queue ID/Expocode (q_id)
        * Filename  (filename)

    *example*:
        {
            "username": "xxxx",
            "hostname": "xxxx",
            "filename": "xxxx",
            "q_id": "xxxx",
        }

**return**:  201 status response

