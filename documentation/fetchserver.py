from flask import Flask 
from flask import request
from flask import render_template
from flask import Response
from flask import json
from flask.ext.sqlalchemy import SQLAlchemy
import os.path

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@127.0.0.1/cchdo'
db = SQLAlchemy(app)

class Fetchlist(db.Model):
    """ Creates reference to MySQL table.

    """
    id = db.Column(db.Integer, primary_key= True)
    queueID = db.Column(db.Integer)
    expocode = db.Column(db.String(80), unique=True)
    path = db.Column(db.String(255))
    queueFile = db.Column(db.String(80))
    fetchedBy = db.Column(db.String(80))
    status = db.Column(db.String(80))

    def __init__(self, fetchedby, path, id= None, expocode=None, queuefile=None):
        """ Match data to respective keywords.

        """
        self.expocode = expocode
        self.queueID = id
        self.path = path
        self.fetchedBy = fetchedby
        self.queueFile = queuefile
        self.status = "Fetched"

    def __repr__(self):
        """ Prints QueueFile.
        
        """
        return '<Queue %r>' %self.QueueFile


@app.route('/', methods=['GET','POST'])
def run():
    """ Runs and renders MySQL data table fetchlist into html on server.

    """
    if request.method == 'POST':
        data = request.json
        username = str(data['username'])+"@"+str(data['hostname'])
        ids = data['q_id']
        expocode = None
        try:
            int(ids)
        except:
            ids = None
            expocode = data['q_id']
        print expocode
        path = data['filename']
        filename = str(os.path.basename(path))
        print filename
        log = Fetchlist(username, path, ids, expocode, filename)
        db.session.add(log)
        db.session.commit()
        
    records = Fetchlist.query.all()
    return render_template('list.html', data = records)

if __name__ == '__main__':
    app.run()
