from flask import Flask, request, render_template, Response, json, jsonify 
from flask.ext.sqlalchemy import SQLAlchemy
import os.path
import urllib import quote_plus as urlquote
from sqlalchemy.engine import create_engine

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://cchdo_web:{0}@ghdc.ucsd.edu/dev_uowtracker'.format(urlquote('((hd0hydr0d@t@'))
db = SQLAlchemy(app)

class Fetchlist(db.Model):
    """ Creates reference to MySQL table.

    """
    id = db.Column(db.Integer, primary_key= True)
    queueID = db.Column(db.Integer)
    expocode = db.Column(db.String(80), unique=True)
    path = db.Column(db.String(255))
    queueFile = db.Column(db.String(80))
    fetchedBy = db.Column(db.String(80))
    status = db.Column(db.String(80))

    def __init__(self, fetchedby, path, status, id= None, expocode=None, queuefile=None):
        """ Match data to respective keywords.

        """
        self.expocode = expocode
        self.queueID = id
        self.path = path
        self.fetchedBy = fetchedby
        self.queueFile = queuefile
        self.status = status

    def __repr__(self):
        """ Prints QueueFile.
        
        """
        return '<Queue %r>' %self.QueueFile


@app.route('/', methods=['GET','POST'])
def run():
    """ Runs and renders MySQL data table fetchlist into html on server.

    """
    if request.method == 'POST':
        data = request.get_json(True)
        print data
        status = 'Fetched'
        print data['commit']
        if data['commit'] == 'yes':
            if data['q_id']:
                qid = data['q_id']
                by_qid = Fetchlist.query.filter_by(queueID = qid).update(dict(status = 'Done'))
            else:
                expo = data['expocode']
            by_expo = Fetchlist.query.filter_by(expocode = expo).update(dict(status = 'Done'))
            db.session.commit()
        else:
            username = str(data['username'])+"@"+str(data['hostname'])
            ids = data['q_id']
            expocode = None
            try:
                int(ids)
            except:
                ids = None
                if len(data['q_id'])==1:
                    expocode = str(data['q_id'][0])
                else:
                    raise IndexError('Multiple Expocodes')
                print expocode
            path = data['filename']
            if path:
                filename = str(os.path.basename(path))
            else:
                filename = None 
            log = Fetchlist(username, path, status, ids, expocode, filename)
            db.session.add(log)
            db.session.commit()
            
    records = Fetchlist.query.all()
    return render_template('list.html', data = records)

if __name__ == '__main__':
    app.run(host= '0.0.0.0', debug=True)
